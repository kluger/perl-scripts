#!/usr/bin/perl -w

############################################################################
# Vigenere translation table
############################################################################
@V=(0x64, 0x73, 0x66, 0x64, 0x3b, 0x6b, 0x66, 0x6f, 0x41, 0x2c, 0x2e,
    0x69, 0x79, 0x65, 0x77, 0x72, 0x6b, 0x6c, 0x64, 0x4a, 0x4b, 0x44,
    0x48, 0x53, 0x55, 0x42, 0x73, 0x67, 0x76, 0x63, 0x61, 0x36, 0x39,
    0x38, 0x33, 0x34, 0x6e, 0x63, 0x78, 0x76, 0x39, 0x38, 0x37, 0x33,
    0x32, 0x35, 0x34, 0x6b, 0x3b, 0x66, 0x67, 0x38, 0x37);
############################################################################
 
############################################################################
# Usage guidelines
############################################################################
# if ($ARGV[0] eq ""){
#    print "This script reveals the IOS passwords obfuscated using the Vigenere algorithm.n";
#    print "n";
#    print "Usage guidelines:n";
#    print " cdecrypt.pl 04480E051A33490E     # Reveals a single passwordn";
#    print " cdecrypt.pl running-config.rcf   # Changes all passwords in a file to cleartextn";
#    print "                                  # Original file stored with .bak extensionn";
# }

 
############################################################################
# Vigenere decryption/deobfuscation function
############################################################################
sub Decrypt{
  my $pw=shift(@_);                             # Retrieve input obfuscated password
  my $i=substr($pw,0,2);                        # Initial index into Vigenere translation table
  my $c=2;                                      # Initial pointer
  my $r="";                                     # Variable to hold cleartext password
  while ($c<length($pw)){                       # Process each pair of hex values
    $r.=chr(hex(substr($pw,$c,2))^$V[$i++]);    # Vigenere reverse translation
    $c+=2;                                      # Move pointer to next hex pair
    $i%=53;                                     # Vigenere table wrap around
  }                                             #
  return $r;                                    # Return cleartext password
}


# my @hahes = qw( 
#    7 144101205C3B29242A3B3C3927
#    7 025017705B3907344E
#    7 10181A325528130F010D24
#    7 124F163C42340B112F3830
#    );


# foreach $_ (@hahes) {
#     print Decrypt($_)."\n";
# }
while (<>) {
    print Decrypt($_)."\n";
}
