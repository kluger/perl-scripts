#!/usr/bin/perl -w
# Email grabber!
# Coded by: Team Arizona
# Website: http://www.arizonainfotech.com

use WWW::Mechanize;
use strict;
use warnings;
use Fcntl ':flock';
use URI::Escape;

my $browser = 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser 1.98.744; .NET CLR 3.5.30729)';
my $connection = WWW::Mechanize->new(agent => $browser, max_redirect => 10,ssl_opts => { verify_hostname => 0 }, requests_redirectable => ['GET', 'POST', 'HEAD']);

$connection->show_progress( 1 );
$connection->timeout(10);
my $google = "https://www.google.ru/search?q="; # The search engine , you can add more , or edit it to another like altavista/search[dot]com/etc ...

my $ext = "php";
my @pre_dorks = (
    # "images",
    # "image",
    # "img",
    # "galery",
    "upload",
    "uploads",
    "cache",
    "report",
    "reports",
    "doc",
    "docs",
    "pic",
    "pics",
    "pictures",
    "install",
    "installer",
);
# my @shellnames =qw(wso shell o x q install tmp a e file set ss test up);
my @shellnames =qw(o x q install tmp a e file set ss test up);

# construction the dorks here ....
my @dorks = ();
foreach my $shellname (@shellnames)
{
    foreach my $probable_folder (@pre_dorks)
    {
        push @dorks, "inurl:${probable_folder}/${shellname}.${ext}";
    }

}

my @pages = (
    "&start=",   "&start=10", "&start=20", "&start=30",
    "&start=40", "&start=50", "&start=60", "&start=70",
    "&start=80", "&start=90", "&start=100"
);


my $file;
request();

sub request {
    for ( my $count = 0; $count <= $#dorks; $count++ ) {
        for ( my $i = 0; $i <= $#pages; $i++ ) {
M:            
            my $url = $google . $dorks[$count] . $pages[$i] ;
            my $response;
            my $res = eval {$response = $connection->get($url); print $response->decoded_dontent(); 1};

            if ( $res )
            {
                foreach my $link ( $connection->find_all_links() ) {
                    my $google_url = $link->url;
                    {
                        my $in;
                        $google_url =~ s/^.*?google\.ru\/url\?//;
                        my @m = split /&/, $google_url;
                        foreach my $t (@m) {
                            my ($k,$v) = split /=/, $t;
                            $in = $v if ($k eq 'url');
                        }
                        if ( defined($in) )
                        {
                            # my $uri     = URI::Encode->new( { encode_reserved => 1 } );
                            # my $encoded = $uri->encode($in);
                            # my $decoded = $uri->decode($in);
                            my $str = uri_escape("$in");
                            print uri_unescape($str);
                            # wf( $file, "$in\n" );
                            $in =~ s/\%([A-Fa-f0-9]{2})/pack('C', hex($1))/seg;
                            $in =~ s/\+/ /g;
                            print "in: $in\nstr: $str\n";
                        }
                    }
                }                
            }
            else
            {
                # goto M;
                next;
            }
            sleep 10;
        }
    }
}

