#!/usr/bin/perl

# twibot.pl script
# (c) eax 2010 | http://eax.me/

# на хостинге у меня нет привилегий суперпользователя,
# потому пришлось поставить модуль из CPAN в свой домашний каталог
use lib "/home/afiskon/perl-modules/lib/perl5/site_perl/5.8.8/";
use Net::Twitter::Lite;
use Encode 'decode';
use strict;

my $login = "afiskon"; # логин в twitter
my $pass = "password"; # пароль
my $cp = "koi8-r"; # в какой кодировке текст скрипта?
my $max_delta = 1000; # насколько м.б. following > followers, можно пересчитать динамически

# TODO - Файлы:
# follow.txt - ники тех, за кем постоянно следим
# waiting.txt - date:id, ожидаем follow-back
# ignore.txt - id и ники тех, за кем не следим ни в коем случае
# block.txt - id и ники тех, кого блочим (и следовательно не следим)

# Логинимся
my $twi = Net::Twitter::Lite->new(
  traits => [qw / API::REST / ],
  username => $login,
  password => $pass,
  clientname => "TwitterFox",
  clientver => "1.8.2",
  clienturl => "http://twitterfox.net/") or die "[-] Login failed: $!\n";

## print "[+] Logged in!\n"; # на самом деле - еще нет ;)

# определяем и выводим текущее время
chomp(my $time = `date +%s`);
print "time=$time:";

# осталось после отладки - получение полной информации о пользователе
#my $info = $twi->show_user("somebody");
#for my $k(keys %$info) {
#  print "$k => ".$info->{$k}."\n";
#}
#exit 0;

my $self = $twi->show_user($login)->{id};
## print "self=$self:";

# Получаем список тех, за кем я слежу
my $following = $twi->following_ids();

# Получаем список тех, кто за мной следит 
my $followers = $twi->followers_ids();

# Выводим их количество
print "following=".(scalar @$following).":";
print "followers=".(scalar @$followers).":";

## print "follow-back ...\n";

# делаем follow-back
my @fb = grep { # "вычитаем" из тех, кто за мной следит, 
  my $t = $_;   # тех, за кем мы уже следим 
  ! grep { $_ == $t } @$following; 
} @$followers; 

# ошибки игнорируем
push @$following, $_ and eval { $twi->create_friend($_) } for (@fb); 
# print "Error (1): $@" if($@);

# выводим число новых following-людей
print "new-following=".@$following.":";

# удаляем из друзей тех, кто отказался дружить с нами в теченее N дней
# UNDER CONSTRUCTION

# объявляем массив наших новых друзей
my @new_friends;

for (@$followers) {
  # кто следит за теми, кто следит за нами и при этом не под нашим наблюдением?
  last unless(@$following - @$followers + @new_friends < $max_delta);
  my $add = $twi->followers_ids($_);

  # удаляем из списка тех, за кем мы следим + самих себя + protected-юзеров, от греха подальше
  # после follow-back @$following включает @$followers
  push @new_friends, grep {
    my $t = $_;
    $t != $self and !grep { $_ == $t } @$following; # and !$twi->show_user($t)->{protected};
  } @$add;
}

@new_friends = @new_friends[0 .. $max_delta - 1 - @$following + @$followers];

# тут может произойти ошибка, если во второй раз попытаться зафрендить protected-юзера
eval { $twi->create_friend($_) } for @new_friends;
# print "Error (2): $@" and exit 2 if($@);

print "new-friends=".@new_friends."\n";
exit 0;