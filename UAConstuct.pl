sub UserAgentConstructor
{
    my @BROWSERS = ('chrome', 'firefox', 'ie', 'opera' );
    my $browser = $BROWSERS[int rand scalar @BROWSERS];

    my %platforms = (
    'Macintosh' => ['68K', 'PPC'],
    'Windows' => ['Win3.11', 'WinNT3.51', 'WinNT4.0', 'Windows NT 5.0', 'Windows NT 5.1', 'Windows NT 5.2', 'Windows NT 6.0', 'Windows NT 6.1', 'Windows NT 6.2', 'Win95', 'Win98', 'Win 9x 4.90', 'WindowsCE'], 
    'X11' => ['Linux i686', 'Linux x86_64' );
    my @VERSIONS = ('1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0', '9.0', '10.0', '11.0', '12.0', '13.0', '14.0', '15.0');
    my @VERSIONS45 = ('4.0', '5.0'); #for Mozilla
    my @langs = qw(en ru ch zw ua cs kz); # for Opera
    my @temp = keys %platforms;
    my $platform = $temp[int rand scalar @temp];
    my $os = $$platforms{$platform}[int rand scalar ($platforms{$platform})];
    
    if ($browser eq 'chrome')
    {
        my $webkit = 500 + int rand 100;
        my $version = int rand 24 . '.0' . int rand 1500 . '.' . int rand 1000;
        return 'Mozilla/5.0 (' . $os . ') AppleWebKit/' . $webkit . '.0 (KHTML, live Gecko) Chrome/' . $version . ' Safari/' . $webkit;
    }
    elsif ($browser eq 'firefox')
    {
        my $year  = 2007 + int rand 8;
        my $month = 1 + int rand 12;
        my $day   = 1 + int rand 30;
        $month    = sprintf( "%02d", $month );
        $day      = sprintf( "%02d", $day );
        my $gecko   = $year . $month . $day;
        my $version = $VERSIONS[ int rand @VERSIONS ];
        my $version45 = $VERSIONS45[ int rand @VERSIONS45 ];
        return 'Mozilla/'. $version45 .' (' . $os . '; rv:' . $version . ') Gecko/' . $gecko . ' Firefox/' . $version;
    }
    elsif ($browser eq 'ie')
    {
        my $version = 1 + int rand 10 . '.0';
        my $engine = 1 + int rand 5 . '.0';
        my $option = int rand 2;
        my $token = '';
        @temp = ('.NET CLR', 'SV1', 'Tablet PC', 'Win64; IA64', 'Win64; x64', 'WOW64');
        if   ( $option > 0 ) { $token = $temp[ int rand @temp ] . '; '; }
        else                 { $token = ''; }
        return 'Mozilla/5.0 (compatible; MSIE ' . $version . '; ' . $os . '; ' . $token . 'Trident/' . $engine . ')';
    }
    elsif ($browser eq 'opera')
    {
        my $browserversion = 7 + int rand 3 . '.80' ;
        my $engine = 10 + int rand 2 . '.0';
        my $lang   = $langs[int rand scalar @langs];
        my $prestoversion = "2.".1 + int rand 13 . '.' . 10 + int rand 120;
        
        return "Opera/$browserversion ($os; U; $lang) Presto/$prestoversion Version/$engine";
    }
}
