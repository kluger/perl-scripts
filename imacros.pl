#!/usr/local/bin/perl

########################################################################################
# PERL WRAPPER FOR iMACRO SCRIPTS
# This code was written by me to be open source. There are no license restrictions.
# - Dave Grossman
########################################################################################
# Version 1.0   29 JAN 2008      Basic demo
########################################################################################

# Make sure to set iMacros browser -> Tools -> Options -> Paths -> FolderDownloads

use Win32::OLE; 

$GlobalMacro = ''; # Global instance of iMacro referred to within the Macros below

sub Main
{
   # Local variables
   local $Macro, $RetCode, $Result;

   #--- iMacros starting --------------
   &StartMacroSession;

   &Macro_Startup;

   &MacroUrl("google.com");   # Go to the Google home page

   $Macro = <<"EOL";
TAG POS=1 TYPE=INPUT:TEXT FORM=NAME:f ATTR=NAME:q CONTENT=iOpus 
TAG POS=1 TYPE=INPUT:SUBMIT FORM=NAME:f ATTR=NAME:btnG  
TAG POS=1 TYPE=A ATTR=HREF:http://www.iopus.com/   
TAG POS=1 TYPE=A ATTR=HREF:http://www.iopus.com/imacros/ 
EOL

   ($RetCode, $Result) = &PlayMacro($Macro);   # Click through to iMacros

   ($RetCode, $Result) = &Macro_ExtractHtmlPage;   # Save the page
   &MacroSaveAs("temp.html");

   &EndMacroSession;
   #--- iMacros ended--------------

   print "$Result\n";   # Print the saved page

   print "\nALL DONE\n";
}

&Main;

exit;

########################################################################################
#--- Basic iMacro functions ----------------------------

sub StartMacroSession
{
   $GlobalMacro = Win32::OLE->new('imacros') or die "Win32:OLE problem\n";
   $GlobalMacro->{Visible} = 1;
   $GlobalMacro->iimInit();
   print "\n***** iMacros started *****\n";
}

sub PlayMacro
{
   local($Macro) = @_;
   local $RetCode = '';
   local $Result = '';
   $Macro =~ s/\s\n/\n/g;      # Eliminate trailing whitespace on each line
   $Macro =~ s/\n/\r\n/g;      # Change line terminator to vbNewLine
   $Macro =~ s/\r\r\n/\r\n/g;   # But be sure not to overdo it
   $RetCode = $GlobalMacro->iimPlay("CODE:$Macro") ;
   if($RetCode < 0) { $Result = $GlobalMacro->iimGetLastError(); }
   else { $Result = $GlobalMacro->iimGetLastExtract; }
   return ($RetCode, $Result);
}

sub EndMacroSession
{
   $GlobalMacro->iimExit();
   print "\n***** iMacros ended *****\n";
}

########################################################################################
# Higher level iMacro functions - all start with "Macro" 
########################################################################################

# BACK - Like clicking the "Back" button on the browser
sub MacroBack { return &PlayMacro("BACK"); }

# FILTERIMAGES - Turns off images on subsequent pages
sub MacroFilterImages { return &PlayMacro("FILTER TYPE=IMAGES STATUS=ON"); }

# LOGIN
sub MacroLogin
{
   local($Url, $UserName, $Password) = @_;
   return &PlayMacro("ONLOGIN USER=$UserName PASSWORD=$Password\nURL GOTO=$Url"); 
}

# ONDOWNLOAD - Sets file store context for next URL
sub MacroOnDownload
{
   local($FileName) = @_;
   return &PlayMacro("ONDOWNLOAD FOLDER=* FILE=$FileName");
}

# PAUSE
sub MacroPause { return &PlayMacro("PAUSE"); }

# REFRESH
sub MacroRefresh { return &PlayMacro("REFRESH"); }

# SAVEAS - Save current page as html
sub MacroSaveAs   # Call WAIT before using this
{
   local($FileName) = @_;
   unlink $FileName;
   return &PlayMacro("SAVEAS TYPE=HTM FOLDER=* FILE=$FileName");
}

# TAB - Only use this browser tab
sub MacroTab { return &PlayMacro("TAB CLOSEALLOTHERS"); }

# TAG - Very complicated command with many parameters. See CheckTagString below for explanation.
sub MacroTag
{
   local($InputString) = @_;
   local $Macro = &CheckTagString($InputString);
   local ($RetCode, $Result) =  &PlayMacro($Macro);
   $Result =~ s/\[EXTRACT\]$//;
   return ($RetCode, $Result); 
}

# URL - Go to URL
sub MacroUrl
{
   local($Url) = @_;
   return &PlayMacro("URL GOTO=$Url");
}

# WAIT
sub MacroWait { return &PlayMacro("WAIT SECONDS=#DOWNLOADCOMPLETE#"); }

########################################################################################
# Still higher level functions - all start with "Macro_" 
########################################################################################

sub Macro_Startup
{
   return &PlayMacro("TAB T=1\nTAB CLOSEALLOTHERS"); 
}

sub Macro_ExtractHtmlPage
{
   $Macro = "TAG POS=1 TYPE=HTML ATTR=*:* EXTRACT=HTM";
   local ($RetCode, $Result) =  &PlayMacro($Macro);
   $Result =~ s/\[EXTRACT\]$//;
   return ($RetCode, $Result); 
}

########################################################################################
# Utility function for TAG 
########################################################################################
# Check TAG string to make sure it looks reasonable
# 
# To choose an item on the page:
#   TYPE   :=X for the container <X>...</X>, e.g., 
#         =A follows links
#         =SELECT
#         =INPUT:TEXT
#         =INPUT:HIDDEN
#
#   ATTR
#      :=LHS:rhs to represent the attribute assignment LHS=rhs (&& boolean allowed), e.g., 
#         =TXT:NameOfLink
#         =HREF:UrlOfLink
#         =ID:Id
#         =VALUE:Value
#   POS
#         =CardinalNumberOfItem with specified TYPE and ATTR
#
# Or to choose a FORM
#   FORM
#      =NAME:NameOfLink
#
# To select from a SELECT list:
#   CONTENT
#      =EVENT:#SAVEITEM
#      =EVENT:#MOUSEOVER
#      =IndexInSELECTlist
#      =$NameInSELECTList
#      =%ValueInSELECTList
# To insert INPUT:
#   CONTENT
#      =ValueToInsert
#
# To extract data (see also SAVEAS):
#   EXTRACT
#      =TXT (eliminates all HTML)
#      =HTM (full html of item)                        
#      =HREF (URL of item)                        
sub CheckTagString
{
   local($InputString) = @_;
   
   # Make associative array of parms and their values
   local @Parms = split(/\s/, $InputString);
   local %Parms = ();
   local $i;
   for($i = 0; $i <= $#Parms; $i++)
   {
      local ($LHS,$RHS) = split(/=/, $Parms[$i]);
      $Parms{$LHS} = $RHS;
   }

   # Unpack the associative array
   local $OutputString = 'TAG';
   local $Error = 0;
   if(exists($Parms{'TYPE'}))
   {
      local $Type = $Parms{'TYPE'};
      $OutputString .= " TYPE=$Type";
       $Pos =~ s/\s//;
      if(length($Type) < 1 || $Type ne $Parms{'TYPE'}) { $Error += 1; }
   }
   if(exists($Parms{'POS'}))
   {
      local $Pos = $Parms{'POS'};
      $OutputString .= " POS=$Pos";
       $Pos =~ tr/0-9//cd;
      if($Pos <= 0 || $Pos ne $Parms{'POS'}) { $Error += 2; }
   }
   if(exists($Parms{'ATTR'}))
   {
      local $Attr = $Parms{'ATTR'};
      $OutputString .= " ATTR=$Attr"; 
      $Attr =~ s/\s//;
      if($Attr ne $Parms{'ATTR'}) { $Error += 4; }
      local @Attr = split(/&&/, $Attr);
      local $i;
      for($i = 0; $i <= $#Attr; $i++)
      {
         local @AttrSubcommand = split(/:/, $Attr[$i]);
         unless (($AttrSubcommand[0] eq 'TXT' || 
            $AttrSubcommand[0] eq 'HREF' ||
            $AttrSubcommand[0] eq 'ID' ||
            $AttrSubcommand[0] eq 'VALUE' ||
            $AttrSubcommand[0] eq '*') 
            && length($AttrSubcommand[1]) > 0)
         { $Error += 8; }
      }
   }
   if(exists($Parms{'FORM'}))
   {
      local $Form = $Parms{'FORM'};
      $OutputString .= " FORM=$Form";
      local @Form = split(/:/, $Form);
      unless ($Form[0] eq 'NAME' && length($Form[1]) > 0) { $Error += 16; }
   }
   if(exists($Parms{'CONTENT'}))
   {
      local $Content = $Parms{'CONTENT'};
      $OutputString .= " CONTENT=$Content";
      local $Leng = length($Content);
      unless((substr($Content, 0, 6) eq 'EVENT:' && $L > 6) || $L > 0) { $Error += 32; }
   }
   if(exists($Parms{'EXTRACT'}))
   {
      local $Extract = $Parms{'EXTRACT'};
      $OutputString .= " EXTRACT=$Extract";
      unless($Extract eq 'TXT' || $Extract eq 'HTM' || $Extract eq 'HREF') { $Error += 64; }
   }

   if($Error > 0) { die "Fatal error $Error in CheckTagString for InputString=$InputString\n"; }
   return $OutputString;
}
