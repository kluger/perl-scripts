#!/usr/bin/perl
use warnings;
#looter 1.58
$count = '100';
$suspect = shift || die "Usage: perl $0 domain\n";
($domain,$domain_tld) = split  (/\./,$suspect);
print "$suspect\n";

$suspect{$suspect}='';

open (DEBUG, ">" . $suspect . "_dbg"  .".txt") or die "Can't write to file [$!]\n";
open (REV,   ">" . $suspect . "_rev"  .".txt") or die "Can't write to file [$!]\n";
open (SUSP,  ">" . $suspect . "_susp" .".txt") or die "Can't write to file [$!]\n";

debug ("[~] start options: perl $0 $suspect\n\n");
rev ("$suspect");
susp ("$suspect");

sub debug {
        (@debug) = @_;
        print DEBUG "@debug\n";
};

sub rev {
        (@rev) = @_;
        print REV "@rev\n";
};

sub susp {
        (@susp) =@_;
        print SUSP "@susp\n";
};

sub dig {
        ($dig) = @_;
        @dig = `dig $dig +short 2>\&1`;
        chomp (@dig);
        debug ("[sub_dig][~req] dig $dig +short 2>\&1");
        debug ("[sub_dig][~reply] @dig");
        @dig = digfail(@dig);
        return @dig;
};

sub digfail {
        @reply = @_;
        foreach $reply (@reply){
                chomp ($reply);
                $label='';
                if ($reply =~m/reached/){
                        $label = '1';
                        last;
                };
                if ($reply =~m/dig: couldn't get address/){
                        $label = '1';
                        last;
                };
        };
        if (!$label){
                return @reply;
        };
        if ($label){
                @reply = ();
                return @reply;
        };
};


sub soa {
        ($host) = @_;
        my @soa = dig ("$host soa");
        foreach $soa (@soa){
                chomp ($soa);
                if ($soa eq ''){
                        next;
                };
                if ($soa eq 'wildcard.vnnic.net.vn.'){
                        next;
                };
                $data{"$host"."_soa_raw"} = "$soa";
                (undef,$host_soa_contact) = split (/ /,$soa);
                $host_soa_contact =~s/\.{1}/@/;
                $host_soa_contact =~s/.$//;
                $data{"$host"."_soa_email"} = "$host_soa_contact";
        };
};

sub whois{
        ($host) = @_;
        debug ("[sub whois] [~req] whois $host 2>&1");
        @whois=`whois $host 2>&1`;
        debug ("[sub whois] [~reply] @whois");
        my $counter ='1';
        foreach $whois (@whois){
                chomp ($whois);
                $data{"$host"."_whois_raw"."$counter"} = "$whois";
                if ($whois =~/([\w\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/){
                        $email = $&;
                        $data{"$host"."_whois_email"."$counter"} = "$email";
                };
                $counter++;
        };
};

sub convert {
        while (($ns,$value) = each %susp_ns){
                @ip = dig("$ns a");
                foreach $ip (@ip){
                        chomp ($ip);
                        print "\t[$ns] to [$ip]\n";
                        $ip{"$ip"} ="$ns.'_'.$value";
                };
        };
};

@ns  = dig ("$suspect ns");

foreach $ns (@ns) {
        chomp ($ns);
        print "\t[$ns]\n";
        ($ns_digit_prefix, $ns_domain) = (split /\./,$ns,2);
        $susp_ns{"$ns"} = "";
        $all_ns{"$ns_domain"} = "$ns_digit_prefix";
};



while (($ns_domain,$ns_digit_prefix) = each %all_ns){
        print "\t domain: [$ns_domain],prefix: [$ns_digit_prefix]\n";
        $wild1 = "pavlushavalera."."$ns_domain";
        $wild2 = "valerapavlusha."."$ns_domain";
        @wild_dig1 = dig ("$wild1 a");
        chomp (@wild_dig1);
        @wild_dig1 = sort (@wild_dig1);
        @wild_dig2 = dig ("$wild2 a");
        chomp (@wild_dig2);
        @wild_dig2 = sort (@wild_dig2);
        if ("@wild_dig1" eq "@wild_dig2"){
                @wild_gen = @wild_dig1;
                foreach $wild_gen (@wild_gen) {
                        chomp ($wild_gen);
                        $wild_gen{$wild_gen} ="$ns_domain";
                        print "\twildcard enabled [$wild1] [$wild_gen]\n\n";
                };
        } else {
                print "wrong wildcard dig!\n";
                print "wild_dig1 [@wild_dig1]\n";
                print "wild_dig2 [@wild_dig2]\n";
        };
        if ($ns_digit_prefix =~/\d$/){
                $ns_prefix = $`;
                for ($i = 1; $i <= $count; $i++) {
                        $ns_gen = "$ns_prefix"."$i."."$ns_domain";
                        $ns_generated{"$ns_gen"} = "";
                };
        };
};



while (($wild,$ns_domain) = each %wild_gen){
        while(($ns) = each %ns_generated){
                @ns_gen_a = dig ("$ns a");
                foreach $ns_gen_a (@ns_gen_a){
                        chomp ($ns_gen_a);
                        if (($ns_gen_a) ne ($wild)){
                                rev ("$ns_gen_a");
                                @check = dig ("\@$ns $suspect +norecurse");
                                foreach $check (@check){
                                        chomp ($check);
                                        if (!exists($all_ns{$check})){
                                                $susp_ns{$ns} = "";
                                        };
                                };
                        };
                };
        };
};

print "susp3\n";
while (($ns_domain) = each %susp_ns){
        print "\t[$ns_domain]\n";
};


@tld = qw (ac ad ae aero af ag ai al am an ao aq ar arpa as asia at au aw ax az ba bb bd be bf bg bh bi biz bj bm bn bo br bs bt bv bw by bz ca
cat cc cd cf cg ch ci ck cl cm cn co com coop cr cu cv cx cy cz de dj dk dm do dz ec edu ee eg er es et eu fi fj fk fm fo fr ga gb gd ge gf gg gh
gi gl gm gn gov gp gq gr gs gt gu gw gy hk hm hn hr ht hu id ie il im in info int io iq ir is it je jm jo jobs jp ke kg kh ki km kn kp kr kw ky kz
la lb lc li lk lr ls lt lu lv ly ma mc md me mg mh mil mk ml mm mn mo mobi mp mq mr ms mt mu museum mv mw mx my mz na name nc ne net nf ng ni nl
no np nr nu nz om org pa pe pf pg ph pk pl pm pn pr pro ps pt pw py qa re ro rs ru rw sa sb sc sd se sg sh si sj sk sl sm sn so sr st su sv sy sz
tc td tel tf tg th tj tk tl tm tn to tp tr travel tt tv tw tz ua ug uk us uy uz va vc ve vg vi vn vu wf ws xxx ye yt za zm zw);


soa ($suspect);
whois ($suspect);

convert;

while (($ns2,$value) = each %ip){
        print "[$ns2]\n";
};
while (($ns2,$value) = each %ip){
        if ($value eq ''){
                print "[$ns2]\n";
        };
        print "look at [$ns2] [$value]\n";
        @dns_wildcard = dig ("disqoteque.disqo \@$ns2 +norecurse");
        if (@dns_wildcard){
                next;
                print "\t[alert] dns server nastroen otdavat otveti na lubuu zonu!\n";
        };
        foreach $tld (@tld){
                chomp ($tld);
                if ($tld eq $domain_tld){
                        next;
                };

                $tldd = "$domain.$tld";

                @good = dig("$tldd \@$ns2 +norecurse");
                if (@good){
                        $label='';
                        foreach $good (@good){
                                chomp ($good);
                                if ($good =~m/\+norecurse/){
                                        $label = '1';
                                        last;
                                };
                        };
                        if (!$label){
                                print "\tfound [$tldd]\n";
                                soa ($tldd);
                                $suspect{$tldd}='';
                                @add_ns  = dig ("$tldd ns");
                                foreach $add_ns (@add_ns){
                                        chomp $add_ns;
                                        $susp_ns{"$add_ns"} = "$tldd\.additional check";
                                        $all_ns{"$add_ns"} = "$tldd\.additional check";
                                };
                                whois ($tldd);
                        };
                };
        };
};


while(($key,$value) = each %data){
        if ($key =~m/_raw/){
                next;
        } else {
                print "\t$key : [$value]\n";
        };
};

convert;

%data_first = %data;
%data = ();


foreach $tld (@tld){
        chomp ($tld);
        if ($tld eq $domain_tld){
                next;
        };

        $tldd = "$domain.$tld";

        @good = dig("$tldd");
        if (@good){
                soa ($tldd);
                whois ($tldd);
        };
};


while(($key,$value) = each %data){
        if ($key =~m/_raw/){
                next;
        } else {
                print "\t$key : [$value]\n";
        };
};



while (($key_first,$value_first) = each %data_first){
        if ($key_first=~m/_raw/){
                next;
        };
        while (($key,$value) = each %data){
                if ($value_first eq $value){
                        if ($key_first ne $key){
                                print "\t[$key_first],[$key],[$value]\n";
                        };
                };
        };
};


while (($ns_recurse) = each %ip){
        print "\tchecking for recursion: - $ns_recurse yahoo.com\n";
        @recurse = dig ("\@$ns_recurse yahoo.com");
        if (@recurse){
                print "\t\trecursion result: [@recurse]\n";
        };
};



while (($ns_axfr) = each %ip){
        foreach $susp(keys %suspect){
                chomp ($susp);
                print "\tdig \@$ns_axfr $susp axfr +short\n";
                @axfr = dig ("\@$ns_axfr $susp axfr");
                debug ("@axfr");
                foreach $axfr (@axfr){
                        chomp $axfr;
                        if ($axfr =~m/Transfer failed/){
                                next;
                        };
                        print "\t\t$axfr\n";
                };
        };
};


while (($ns_axfr) = each %ip){
        foreach $susp(keys %suspect){
                chomp ($susp);
                print "\@$ns_axfr $susp\n";

                @soa = dig ("\@$ns_axfr $susp soa");
                if (@soa) {
                        print "\t[soa] [@soa]\n";
                };
                @a = dig ("\@$ns_axfr $susp a");
                if (@a){
                        print "\t[a] [@a]\n";
                };
                @mx = dig ("\@$ns_axfr $susp mx");
                if (@mx){
                        print "\t[mx] [@mx]\n";
                };
                @txt = dig ("\@$ns_axfr $susp txt ");
                if (@txt){
                        print "\t[txt] [@txt]\n";
                };
                @shit = dig ("\@$ns_axfr $susp shit");
                if (@shit){
                        print "\t[shit] [@shit]\n";
                };
                @any = dig ("\@$ns_axfr $susp any");
                if (@any){
                        print "\t[any] [@any]\n";
                };
                @x25 = dig ("\@$ns_axfr $susp x25");
                if (@x25){
                        print "[x25] [@x25]\n";
                };
                @wks = dig ("\@$ns_axfr $susp wks ");
                if (@wks){
                        print "[wks] [@wks]\n";
                };
                @tsig = dig ("\@$ns_axfr $susp tsig ");
                if (@tsig){
                        print "[tsig] [@tsig]\n";
                };
                @tkey = dig ("\@$ns_axfr $susp tkey ");
                if (@tkey) {
                        print "[tkey] [@tkey]\n";
                };
                @sig = dig ("\@$ns_axfr $susp sig ");
                if (@sig) {
                        print "[sig] [@sig]\n";
                };
                @rt = dig ("\@$ns_axfr $susp rt ");
                if (@rt){
                        print "[rt] [@rt]\n";
                };
                @rrsig = dig ("\@$ns_axfr $susp rrsig ");
                if (@rrsig){
                        print "[rrsig] [@rrsig]\n";
                };
                @rp = dig ("\@$ns_axfr $susp rp ");
                if (@rp) {
                        print "[rp] [@rp]\n";
                };
                @px = dig ("\@$ns_axfr $susp px ");
                if (@px){
                        print "[px] [@px]\n";
                };
                @ptr = dig ("\@$ns_axfr $susp ptr ");
                if (@ptr){
                        print "[ptr] [@ptr]\n";
                };
                @nxt = dig ("\@$ns_axfr $susp nxt ");
                if (@nxt){
                        print "[nxt] [@nxt]\n";
                };
                @nsec3param = dig ("\@$ns_axfr $susp nsec3param ");
                if (@nsec3param){
                        print "[nsec3param] [@nsec3param]\n";
                };
                @nsec3 = dig ("\@$ns_axfr $susp nsec3 ");
                if (@nsec3){
                        print "[nsec3] [@nsec3]\n";
                };
                @nsec = dig ("\@$ns_axfr $susp nsec ");
                if (@nsec){
                        print "[nsec] [@nsec]\n";
                };
                @nsapptr = dig ("\@$ns_axfr $susp nsap-ptr ");
                if (@nsapptr){
                        print "[nsapptr] [@nsapptr]\n";
                };
                @nsap = dig ("\@$ns_axfr $susp nsap ");
                if (@nsap){
                        print "[nsap] [@nsap]\n";
                };
                @null = dig ("\@$ns_axfr $susp null ");
                if (@null){
                        print "[null] [@null]\n";
                };
                @naptr = dig ("\@$ns_axfr $susp naptr ");
                if (@naptr){
                        print "[naptr] [@naptr]\n";
                };
                @mr = dig ("\@$ns_axfr $susp mr ");
                if (@mr){
                        print "[mr] [@mr]\n";
                };
                @minfo = dig ("\@$ns_axfr $susp minfo ");
                if (@minfo){
                        print "[minfo] [@minfo]\n";
                };
                @mg = dig ("\@$ns_axfr $susp mg ");
                if (@mg){
                        print "[mg] [@mg]\n";
                };
                @mf = dig ("\@$ns_axfr $susp mf ");
                if (@mf){
                        print "[mf] [@mf]\n";
                };
                @md = dig ("\@$ns_axfr $susp md ");
                if (@md){
                        print "[md] [@md]\n";
                };
                @mb = dig ("\@$ns_axfr $susp mb ");
                if (@mb){
                        print "[mb] [@mb]\n";
                };
                @loc = dig ("\@$ns_axfr $susp loc ");
                if (@loc){
                        print "[loc] [@loc]\n";
                };
                @kx = dig ("\@$ns_axfr $susp kx ");
                if (@kx){
                        print "[kx] [@kx]\n";
                };
                @key = dig ("\@$ns_axfr $susp key ");
                if (@key){
                        print "[key] [@key]\n";
                };
                @isdn = dig ("\@$ns_axfr $susp isdn ");
                if (@isdn){
                        print "[isdn] [@isdn]\n";
                };
                @gpos = dig ("\@$ns_axfr $susp gpos ");
                if (@gpos){
                        print "[gpos] [@gpos]\n";
                };
                @ds = dig ("\@$ns_axfr $susp ds ");
                if (@ds){
                        print "[ds] [@ds]\n";
                };
                @dnskey = dig ("\@$ns_axfr $susp dnskey ");
                if (@dnskey){
                        print "[dnskey] [@dnskey]\n";
                };
                @dname = dig ("\@$ns_axfr $susp dname ");
                if (@dname){
                        print "[dname] [@dname]\n";
                };
                @cname = dig ("\@$ns_axfr $susp cname ");
                if (@cname){
                        print "[cname] [@cname]\n";
                };
                @afsdb = dig ("\@$ns_axfr $susp afsdb ");
                if (@afsdb){
                        print "[afsdb] [@afsdb]\n";
                };
                @aaaa = dig ("\@$ns_axfr $susp aaaa ");
                if (@aaaa){
                        print "\t[aaaa] [@aaaa]\n";
                };
                @a6 = dig ("\@$ns_axfr $susp a6 ");
                if (@a6){
                        print "[a6] [@a6]\n";
                };
                @srv = dig ("\@$ns_axfr $susp srv ");
                if (@srv){
                        print "[srv] [@srv]\n";
                };

                @hinfo = dig ("\@$ns_axfr $susp hinfo");
                if (@hinfo){
                        print "[hinfo] [@hinfo]\n";
                };
                print "\n";
        };
};


while (($ns_config) = each %ip){
        @version =  dig ("\@$ns_config version.bind txt chaos");
        @hostname = dig ("\@$ns_config hostname.bind txt chaos");
        @authors = dig ("\@$ns_config authors.bind");
        print "$ns_config [@version] @ [@hostname]\n";
        if (@authors){
                print "authors: [@authors]\n";
        };
};