#!/usr/bin/perl
use LWP::Simple;
use LWP::UserAgent;
use HTTP::Request;
use HTTP::Response;
use HTML::LinkExtor;
use warnings;
use strict;
use utf8;
use threads;
use threads::shared;
use Fcntl ':flock';

# Url to start at;
my $URL = "http://www.kleos.ru/";
# $URL = "http://forum.xakep.ru/m_1487541/tm.htm"; #url_for test
# битые ссылки будут выводиться в консоль и в файл $file
my $file ="badlinks.txt";
# количество просматриваемых ссылок, найденных на сайте
my $stop_it =1000;
# количество потоков, в которых запускается поиск (не рекомендую не более 50 на i5)
my $threads = 5;
$0 = "my_best_crawler";
my @urls : shared;
for ( my $i = 0; $i < $threads; $i++ ) {
    push @urls, $URL;
}
my $url_count : shared = 0;
my %visited : shared;
my @thr;


# пример проверки запуска одного экземпляра скрипта.
my $start = `pidof $0`;
if ($start =~ m/^(\d+)(\s(\d+))+/){
    die "not started! $0 script exist, pid: $start";
}




for my $t (1..$threads) {
    
    push @thr, threads->create({ 'context'     => 'list',
                                 'stack_size'  => 32*4096,
                                 'exit'        => 'thread_only'
                                    }, \&getter, $t);
}

foreach my $t (@thr) {
  $t->join();
  
}

sub getter {
    my $th = shift;
    # sleep ($th + 3); # с течением времени отработает первый поток и в массиве будут различные ссылки
    print "Thread $th started\n";
    while ( scalar @urls ) {
        my $url = shift @urls;
        if ( exists $visited{$url} )    ##check if URL already exists
        {
            next;
        }
        else {
            $url_count++;
        }
        my $ua = LWP::UserAgent->new;
        push @{ $ua->requests_redirectable }, ('GET', 'POST');
        $ua->agent("Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.19) Gecko/2010031422 Firefox/3.0.19");
        $ua->timeout(20);
        $ua->show_progress(1);
        my $response = $ua->get($url);


        if ( $response->is_error() ) {
            printf "$th BAD %s %s\n", $response->code,$url;
            print $url."\n";
            wf( $file, "$url\n" );
            $visited{$url} = "error";
        }
        else {
            # printf "$th GOOD %s %s\n", $response->code,$url;
            $visited{$url} = "good";
        }
        my $contents = $response->content();

        my ($page_parser) = HTML::LinkExtor->new( undef, $URL );
        $page_parser->parse($contents)->eof;
        my @links = $page_parser->links;

        foreach my $link (@links) {
            # надо исключить ссылки типа mail:to, здесь можно задать фильтр выбора в виде регулярки.
            if ( $$link[2] =~ m@(((http\:\/\/)|(www\.))([a-z]|[A-Z]|[0-9]|[/.]|[~]|[-_]|[()])*[^'">])@i )
            {
                # print "$$link[2]\n";
                push @urls, "$$link[2]";
            }
        }
        if ( $url_count >= $stop_it ) {
            print $url_count. " visited. STOP\n";
            last;
        }
    }
}


sub wf
{
    open my $dat, '>>', $_[0] or die "\nCould not open $_[0] file!\n";
    flock $dat, LOCK_EX;
    print $dat $_[1];
    flock $dat, LOCK_UN;
    close $dat;
}